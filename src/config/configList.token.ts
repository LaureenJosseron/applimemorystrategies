import { InjectionToken, ValueProvider } from "@angular/core";
import { configListData } from "./configList";

export interface configList {

    GroupementCategoriel: {
        images: {
            background0: string[],
            familiarisation1: Record<number, string[]>,
            familiarisation2: Record<number, string[]>,
            liste1: Record<number, string[]>,
            liste2: Record<number, string[]>,
            liste3: Record<number, string[]>
        },
        parametres: {
            imageNumber: number,
            typeliste: keyof typeof configListData.GroupementCategoriel.images,
            time: number,
            isButtonGreen: boolean

        }
    },
    AutoRepetition: {
        images: {
            familiarisation1: string[],
            familiarisation2: string[],
            liste1: string[],
            liste2: string[],
            liste3: string[],
        },
        parametres: {
            familiarisation1: {
                imageNumber: number,
                timeDisplay: number,
                timeTempoBetweenCard: number,
                autoRotate: boolean,
                randomImg: boolean,
                autoRestitution: boolean,
                timeRestitution: number,
                colorchange: boolean,
                orderimage: number
            },
            familiarisation2: {
                imageNumber: number,
                timeDisplay: number,
                timeTempoBetweenCard: number,
                autoRotate: boolean,
                randomImg: boolean,
                autoRestitution: boolean,
                timeRestitution: number,
                colorchange: boolean,
                orderimage: number
            },
            liste1: {
                imageNumber: number,
                timeDisplay: number,
                timeTempoBetweenCard: number,
                autoRotate: boolean,
                randomImg: boolean,
                autoRestitution: boolean,
                timeRestitution: number,
                colorchange: boolean,
                orderimage: number
            },
            liste2: {
                imageNumber: number,
                timeDisplay: number,
                timeTempoBetweenCard: number,
                autoRotate: boolean,
                randomImg: boolean,
                autoRestitution: boolean,
                timeRestitution: number,
                colorchange: boolean,
                orderimage: number
            },
            liste3: {
                imageNumber: number,
                timeDisplay: number,
                timeTempoBetweenCard: number,
                autoRotate: boolean,
                randomImg: boolean,
                autoRestitution: boolean,
                timeRestitution: number,
                colorchange: boolean,
                orderimage: number
            }
        }
    },
    MemoireSelective: {
        images: {
            liste1: Record<number, string[]>,
            liste2: Record<number, string[]>,
        },
        parametres: {
            typeliste: keyof typeof configListData.MemoireSelective.images,
            type: string,
            time: number
        }
    };
}

export const CONFIGLIST = new InjectionToken("configListData");

export const getConfigListProvider = (value: typeof configListData): ValueProvider => ({
    provide: CONFIGLIST,
    useValue: value
})
