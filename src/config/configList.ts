export const configListData = {
    GroupementCategoriel : {
        images : {
            background0 : [
                "assets/GroupementCategoriel/Background/BackG1.jpg",
                "assets/GroupementCategoriel/Background/BackG2.jpg",
                "assets/GroupementCategoriel/Background/BackG3.jpg",
                "assets/GroupementCategoriel/Background/BackG4.jpg",
            ],
            familiarisation1 : {
                0 : [
                    "assets/GroupementCategoriel/familiarisation1/outils/clou.jpg",
                    "assets/GroupementCategoriel/familiarisation1/outils/hache.jpg",
                    "assets/GroupementCategoriel/familiarisation1/outils/marteau.jpg",
                    "assets/GroupementCategoriel/familiarisation1/outils/scie.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/familiarisation1/partiescorps/bras.jpg",
                    "assets/GroupementCategoriel/familiarisation1/partiescorps/jambe.jpg",
                    "assets/GroupementCategoriel/familiarisation1/partiescorps/main.jpg",
                    "assets/GroupementCategoriel/familiarisation1/partiescorps/pied.jpg",
                ],
            },
            familiarisation2 : {
                0 : [
                    "assets/GroupementCategoriel/familiarisation2/animaux/elephant.jpg",
                    "assets/GroupementCategoriel/familiarisation2/animaux/girafe.jpg",
                    "assets/GroupementCategoriel/familiarisation2/animaux/lion.jpg",
                    "assets/GroupementCategoriel/familiarisation2/animaux/singe.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/familiarisation2/formes/coeur.jpg",
                    "assets/GroupementCategoriel/familiarisation2/formes/etoile.jpg",
                    "assets/GroupementCategoriel/familiarisation2/formes/fleche.jpg",
                    "assets/GroupementCategoriel/familiarisation2/formes/lune.jpg",
                ],
                2: [
                    "assets/GroupementCategoriel/familiarisation2/jeux/ballon.jpg",
                    "assets/GroupementCategoriel/familiarisation2/jeux/cartes.jpg",
                    "assets/GroupementCategoriel/familiarisation2/jeux/de.jpg",
                    "assets/GroupementCategoriel/familiarisation2/jeux/poupee.jpg",
                ]
            },
            liste1 : {
                0 : [
                    "assets/GroupementCategoriel/liste1/animaux/chat.jpg",
                    "assets/GroupementCategoriel/liste1/animaux/cheval.jpg",
                    "assets/GroupementCategoriel/liste1/animaux/chien.jpg",
                    "assets/GroupementCategoriel/liste1/animaux/lapin.jpg",
                    "assets/GroupementCategoriel/liste1/animaux/tortue.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/liste1/fruits/banane.jpg",
                    "assets/GroupementCategoriel/liste1/fruits/cerise.jpg",
                    "assets/GroupementCategoriel/liste1/fruits/citron.jpg",
                    "assets/GroupementCategoriel/liste1/fruits/poire.jpg",
                    "assets/GroupementCategoriel/liste1/fruits/pomme.jpg",
                ],
                2 : [
                    "assets/GroupementCategoriel/liste1/vehicules/avion.jpg",
                    "assets/GroupementCategoriel/liste1/vehicules/bus.jpg",
                    "assets/GroupementCategoriel/liste1/vehicules/camion.jpg",
                    "assets/GroupementCategoriel/liste1/vehicules/moto.jpg",
                    "assets/GroupementCategoriel/liste1/vehicules/voiture.jpg",
                ],
                3 : [
                    "assets/GroupementCategoriel/liste1/partiescorps/doigt.jpg",
                    "assets/GroupementCategoriel/liste1/partiescorps/levres.jpg",
                    "assets/GroupementCategoriel/liste1/partiescorps/nez.jpg",
                    "assets/GroupementCategoriel/liste1/partiescorps/oeil.jpg",
                    "assets/GroupementCategoriel/liste1/partiescorps/oreille.jpg",
                ]
            
            }, 
            liste2 : {
                0 : [
                    "assets/GroupementCategoriel/liste2/animaux/elephant.jpg",
                    "assets/GroupementCategoriel/liste2/animaux/girafe.jpg",
                    "assets/GroupementCategoriel/liste2/animaux/lion.jpg",
                    "assets/GroupementCategoriel/liste2/animaux/ours.jpg",
                    "assets/GroupementCategoriel/liste2/animaux/singe.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/liste2/outils/clou.jpg",
                    "assets/GroupementCategoriel/liste2/outils/hache.jpg",
                    "assets/GroupementCategoriel/liste2/outils/marteau.jpg",
                    "assets/GroupementCategoriel/liste2/outils/vis.jpg",
                    "assets/GroupementCategoriel/liste2/outils/scie.jpg",
                ],
                2 : [
                    "assets/GroupementCategoriel/liste2/meubles/canape.jpg",
                    "assets/GroupementCategoriel/liste2/meubles/chaise.jpg",
                    "assets/GroupementCategoriel/liste2/meubles/lit.jpg",
                    "assets/GroupementCategoriel/liste2/meubles/table.jpg",
                    "assets/GroupementCategoriel/liste2/meubles/tabouret.jpg",
                ],
                3 : [
                    "assets/GroupementCategoriel/liste2/partiescorps/bras.jpg",
                    "assets/GroupementCategoriel/liste2/partiescorps/jambe.jpg",
                    "assets/GroupementCategoriel/liste2/partiescorps/pied.jpg",
                    "assets/GroupementCategoriel/liste2/partiescorps/cheveux.jpg",
                    "assets/GroupementCategoriel/liste2/partiescorps/main.jpg",
                ]

            },
            liste3 : {
                0 : [
                    "assets/GroupementCategoriel/liste3/jeux/ballon.jpg",
                    "assets/GroupementCategoriel/liste3/jeux/cartes.jpg",
                    "assets/GroupementCategoriel/liste3/jeux/cerfvolant.jpg",
                    "assets/GroupementCategoriel/liste3/jeux/poupee.jpg",
                    "assets/GroupementCategoriel/liste3/jeux/raquette.jpg",
                ],
                1 : [
                    "assets/GroupementCategoriel/liste3/objetscuisine/casserole.jpg",
                    "assets/GroupementCategoriel/liste3/objetscuisine/cuillere.jpg",
                    "assets/GroupementCategoriel/liste3/objetscuisine/fourchette.jpg",
                    "assets/GroupementCategoriel/liste3/objetscuisine/verre.jpg",
                    "assets/GroupementCategoriel/liste3/objetscuisine/bol.jpg",
                ],
                2 : [
                    "assets/GroupementCategoriel/liste3/instruments/guitare.jpg",
                    "assets/GroupementCategoriel/liste3/instruments/piano.jpg",
                    "assets/GroupementCategoriel/liste3/instruments/tambour.jpg",
                    "assets/GroupementCategoriel/liste3/instruments/trompette.jpg",
                    "assets/GroupementCategoriel/liste3/instruments/violon.jpg",
                ],
                3 : [
                    "assets/GroupementCategoriel/liste3/vetements/chaussette.jpg",
                    "assets/GroupementCategoriel/liste3/vetements/chaussure.jpg",
                    "assets/GroupementCategoriel/liste3/vetements/pull.jpg",
                    "assets/GroupementCategoriel/liste3/vetements/pantalon.jpg",
                    "assets/GroupementCategoriel/liste3/vetements/robe.jpg",
                ]
            }
        },
        parametres : {
            imageNumber : 20,
            typeliste : "liste1",
            time : 8,
            backGr : false
        }
    },
    AutoRepetition : {
        images : {
            familiarisation1:[
                "assets/AutoRepetition/familiarisation1/chaise.jpg",
                "assets/AutoRepetition/familiarisation1/zebre.jpg",
            ],
            familiarisation2:[
                "assets/AutoRepetition/familiarisation2/moto.jpg",
                "assets/AutoRepetition/familiarisation2/poisson.jpg",
                "assets/AutoRepetition/familiarisation2/stylo.jpg",
            ],
            liste1 : [
                "assets/AutoRepetition/liste1/avion.jpg",
                "assets/AutoRepetition/liste1/feuille.jpg",
                "assets/AutoRepetition/liste1/livre.jpg",
                "assets/AutoRepetition/liste1/oiseau.jpg",
                "assets/AutoRepetition/liste1/pomme.jpg",
                "assets/AutoRepetition/liste1/table.jpg",
            ], 
            liste2 : [
                "assets/AutoRepetition/liste2/arbre.jpg",
                "assets/AutoRepetition/liste2/bouton.jpg",
                "assets/AutoRepetition/liste2/chapeau.jpg",
                "assets/AutoRepetition/liste2/lapin.jpg",
                "assets/AutoRepetition/liste2/montre.jpg",
                "assets/AutoRepetition/liste2/velo.jpg",
            ],
            liste3 : [
                "assets/AutoRepetition/liste3/balai.jpg",
                "assets/AutoRepetition/liste3/camion.jpg",
                "assets/AutoRepetition/liste3/marteau.jpg",
                "assets/AutoRepetition/liste3/piano.jpg",
                "assets/AutoRepetition/liste3/regle.jpg",
                "assets/AutoRepetition/liste3/tortue.jpg",
            ]
        },
        parametres: {
            familiarisation1: {
                imageNumber: 2,
                timeDisplay: Infinity,
                timeTempoBetweenCard: 0,
                autoRotate: false,
                randomImg: false,
                autoRestitution: false,
                timeRestitution: 5,
                colorchange: false,
                orderimage: 1
            },
            familiarisation2: {
                imageNumber: 3,
                timeDisplay: 4,
                timeTempoBetweenCard: 4,
                autoRotate: false,
                randomImg: false,
                autoRestitution: false,
                timeRestitution: 5,
                colorchange: false,
                orderimage: 1
            },
            liste1: {
                imageNumber: 6,
                timeDisplay: 2,
                timeTempoBetweenCard: 2,
                autoRotate: false,
                randomImg: false,
                autoRestitution: false,
                timeRestitution: 5,
                colorchange: false,
                orderimage: 1
            },
            liste2: {
                imageNumber: 6,
                timeDisplay: 2,
                timeTempoBetweenCard: 2,
                autoRotate: false,
                randomImg: false,
                autoRestitution: false,
                timeRestitution: 5,
                colorchange: false,
                orderimage: 1
            },
            liste3: {
                imageNumber: 6,
                timeDisplay: 2,
                timeTempoBetweenCard: 2,
                autoRotate: false,
                randomImg: false,
                autoRestitution: false,
                timeRestitution: 5,
                colorchange: false,
                orderimage: 1
            }

        }
    },
    MemoireSelective: {
        images: {
            liste1: {
                0: [
                    "assets/MemoireSelective/liste1/cage/canard.jpg",
                    "assets/MemoireSelective/liste1/cage/cheval.jpg",
                    "assets/MemoireSelective/liste1/cage/papillon.jpg",
                    "assets/MemoireSelective/liste1/cage/poisson.jpg",
                    "assets/MemoireSelective/liste1/cage/singe.jpg",
                    "assets/MemoireSelective/liste1/cage/souris.jpg",
                ],
                1: [
                    "assets/MemoireSelective/liste1/maison/chaise.jpg",
                    "assets/MemoireSelective/liste1/maison/ciseau.jpg",
                    "assets/MemoireSelective/liste1/maison/guitare.jpg",
                    "assets/MemoireSelective/liste1/maison/livre.jpg",
                    "assets/MemoireSelective/liste1/maison/porte.jpg",
                    "assets/MemoireSelective/liste1/maison/verre.jpg",
                ],

            },
            liste2: {
                0: [
                    "assets/MemoireSelective/liste2/cage/chat.jpg",
                    "assets/MemoireSelective/liste2/cage/grenouille.jpg",
                    "assets/MemoireSelective/liste2/cage/lapin.jpg",
                    "assets/MemoireSelective/liste2/cage/ours.jpg",
                    "assets/MemoireSelective/liste2/cage/tortue.jpg",
                    "assets/MemoireSelective/liste2/cage/vache.jpg",
                ],
                1: [
                    "assets/MemoireSelective/liste2/maison/balai.jpg",
                    "assets/MemoireSelective/liste2/maison/bouteille.jpg",
                    "assets/MemoireSelective/liste2/maison/enveloppe.jpg",
                    "assets/MemoireSelective/liste2/maison/fourchette.jpg",
                    "assets/MemoireSelective/liste2/maison/lampe.jpg",
                    "assets/MemoireSelective/liste2/maison/passoire.jpg",
                ],

            },

        },
        parametres: {
            typeliste: "liste1",
            type: "CMCM",
            time: 0.5
        }

    }
} 