import { ExcelComponent } from 'src/app/shared/excel/excel.component';
import { Component, Input } from '@angular/core';
import {MatDialog, MatDialogRef} from '@angular/material/dialog';
import { Router } from '@angular/router';
import Utils from '../utils/utils';
import { DialogContentComponent } from './dialog-content.component';

@Component({
  selector: 'app-modal-escape',
  templateUrl: './modal-escape.component.html',
  styleUrls: ['./modal-escape.component.scss']
})
export class ModalEscapeComponent{

  @Input() route!: string;
  @Input() data: any = {};  
  @Input() timer: any;
  @Input() nameParticipant: string = "";
  
  dialogRef!:MatDialogRef<DialogContentComponent> | undefined;

  constructor(public dialog: MatDialog, private router: Router) {}

  ExcelComponent: ExcelComponent = new ExcelComponent();
  /**
  * Function will open the escape dialog 
  * @param / 
  * @returns {void}
  */
  openDialog() {
    if (this.dialogRef == undefined){
      this.dialogRef = this.dialog.open(DialogContentComponent, {
        disableClose: true,
        hasBackdrop: true,
        data : this.route
      });

      this.dialogRef.afterClosed().subscribe((result :any) => {
        if(result == "backMenuAndSave"){
          Utils.downloadData(this.data, this.nameParticipant);
          // if(this.route="/groupementcategoriel"){
            Utils.setExcelComponent(this.ExcelComponent);
          // }
          Utils.downloadExcel(this.nameParticipant);
          if(this.timer)
            this.timer.removeTimer();
          this.router.navigate([this.route]);
        }
        else if(result == "backMenu"){
          if(this.timer)
            this.timer.removeTimer();
          this.router.navigate([this.route]);
        }
        if(this.timer)
          this.timer.start();
        this.dialogRef = undefined;
      });

    }
  }

}
