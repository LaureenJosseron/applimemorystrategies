import { Component, Inject } from "@angular/core";
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
    selector: 'app-dialog-content',
    templateUrl: './dialog-content.component.html',
    styleUrls: ['./dialog-content.component.scss']
  })
export class DialogContentComponent {
  
  public displaySaveButton : boolean = false;

  constructor(public dialogRef: MatDialogRef<DialogContentComponent>, @Inject(MAT_DIALOG_DATA) public data: string) { 
    if(data == "/groupementcategoriel"){
      this.displaySaveButton = true;
    }
  }

  /**
  * Function will close the escape dialog 
  * @param {string} state data sent to the escape dialog 
  * @returns {void}
  */
  close(state: string): void {
    this.dialogRef.close(state);
  }  
}