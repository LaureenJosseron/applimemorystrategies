import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MainButtonComponent } from './main-button/main-button.component';
import { ModalEscapeComponent } from './modal-escape/modal-escape.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogContentComponent } from './modal-escape/dialog-content.component';
import { ExcelComponent } from './excel/excel.component';

@NgModule({
    declarations: [
        MainButtonComponent,
        ModalEscapeComponent,
        DialogContentComponent,
        ExcelComponent
    ],
    imports: [
        CommonModule,
        MatButtonModule,
        MatDialogModule,
        
    ],
    exports: [
        MainButtonComponent,
        ModalEscapeComponent,
        DialogContentComponent,
        ExcelComponent
    ],
})
export class SharedModule { }
