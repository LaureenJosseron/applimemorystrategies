import { ExcelComponent } from 'src/app/shared/excel/excel.component';
import html2canvas from "html2canvas";

export default class Utils {
  static ExcelComponent: ExcelComponent;

  static setExcelComponent(excelComponent: ExcelComponent) {
    this.ExcelComponent = excelComponent;
  }

  /**
  * Function will create and download files
  * @param {any} myJson data to insert into the file
  * @param {string} name name of the file 
  * @returns {void}
  */
  static downloadData(myJson : any, name  : string){
    html2canvas(document.body).then(canvas => {
      this.downloadFile(canvas.toDataURL(), ".png", name);
    });

    var sJson = JSON.stringify(myJson, null, 2);
    let href = "data:text/json;charset=UTF-8," + encodeURIComponent(sJson);
    this.downloadFile(href, ".json", name);
  }

  /**
  * Function will create element of dom for download
  * @param {string} href link for download file
  * @param {string} fileext extension of file
  * @param {string} name name of the file 
  * @returns {void}
  */
  static downloadFile(href: string, fileext : string, name: string){
    var element = document.createElement('a');
    element.setAttribute('href', href);
    element.setAttribute('download', name + this.getDatetoString() + fileext);
    element.style.display = 'none';
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
  }
  
  /**
   * Function will create suffle table of images
   * @param {any} tab - table of images
   * @param {number} nbImages - number of images
   * @param {number} nbCasesX - number of case of axe X of the screen
   * @returns {(String | Number)[]}
   */
  static createShuffleTabImages(tab : any, nbImages:number, nbCasesX: number) : (String | Number)[]{
    let cloneTab : (String | Number)[] = [];
    let data  = [];
    let errorTab = true;

    while(errorTab){
      errorTab = false;
      cloneTab = [...tab];
      data = [{index : 0, count : 0},{index : 1, count : 0},{index : 2, count : 0},{index : 3, count : 0}];

      for(let i = 0; i<cloneTab.length; i++){
        if (cloneTab[i] == 5) {
          let t = [...new Set(this.arrayAdjacentCase(cloneTab, i, nbCasesX).filter(item => item !== ""))];
          let difference = data.filter(x => x.count < nbImages/4).map(x => x).filter(x => !t.includes(x.index));
          if(difference.length == 0){
            errorTab = true;
            break;
          }
          let item = difference[this.getRandom(difference.length)];
          cloneTab[i] = item.index;
          item.count++;
        }
      }
    }
    return cloneTab;
  }

  /**
   * Function will shuffle table
   * @param {any} array - table for shuffle
   * @returns {any} 
   */
  static shuffleArray(array : any){
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
    return array;
  }

  /**
   * Function will return ramdom numer
   * @param {number} val - interval of number
   * @returns {number}
   */
  static getRandom(val : number) {
    return Math.floor(Math.random() * val);
  }

  /**
   * Function will create an array for all adjacent cases
   * @param {any} tab - table of cases
   * @param {any} index - index of case
   * @param {number} nbCasesX - number of case of axe X of the screen
   * @returns {any}
   */
  static arrayAdjacentCase(tab : any, index : any, nbCasesX : number = 11){
    let array = [];

    index = index - nbCasesX;
    array.push(tab[index - 1]);
    array.push(tab[index]);
    array.push(tab[index + 1]);

    index = index + nbCasesX;
    array.push(tab[index - 1]);
    array.push(tab[index + 1]);

    index = index + nbCasesX;
    array.push(tab[index - 1]);
    array.push(tab[index]);
    array.push(tab[index + 1]);

    return array;
  }

  /**
   * Function getter of now date in string
   * @returns {string}
   */
  static getDatetoString(){
    let now = new Date();
    let date = now.toLocaleDateString();
    let time = now.toLocaleTimeString();
    let filename = "_" + date.split('/').join('-') + "_" + time.split(':').join('-');
    return filename;
  }

  /**
   * Function will create promise for synchrone tempo
   * @param {any} time  - time in minutes
   * @returns {promises}
   */
  static promiseTempo(time : any){
    return new Promise(resolve =>
    setTimeout(() => resolve(''), time * 1000)
    );
  }

   /**
   * Function will create promise for synchrone tempo
   * @param {any} time  - time in minutes
   * @returns {promises}
   */
   static downloadExcel(name: string){
    let date=Utils.getDatetoString();
    this.ExcelComponent.fileName=name+date+'.xlsx';  
    this.ExcelComponent.exportexcel();
    
  }


}