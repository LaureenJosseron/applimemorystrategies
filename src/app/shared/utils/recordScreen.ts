import Utils from "./utils"

export default class RecordScreen {
  nameParticipant : string | null | undefined = "";
  stream : any;
  mime : any;
  mediaRecorder : any;
  chunks : any = []

  constructor(){}

  async init(){
    this.stream = await navigator.mediaDevices.getDisplayMedia({
      video: true
    })
  
    //needed for better browser support
    this.mime = MediaRecorder.isTypeSupported("video/webm; codecs=vp9") 
                ? "video/webm; codecs=vp9" 
                : "video/webm"
    this.mediaRecorder = new MediaRecorder(this.stream, {
        mimeType: this.mime
    })
  }

  /**
   * Function asynchrone - start the record
   */
  async start(){
    await this.init();
    this.mediaRecorder.addEventListener('dataavailable', (e : any) => {
        this.chunks.push(e.data)
    });

    //we have to start the recorder manually
    this.mediaRecorder.start();
    this.stop();
  }
  /**
   * Function stop the record
   */
  stop() : void {
    this.mediaRecorder.addEventListener('stop', () => {
      let blob = new Blob(this.chunks, {
          type: this.chunks[0].type
      })
      let url = URL.createObjectURL(blob)

      let video : any = document.querySelector("video")
      video.src = url

      let a = document.createElement('a')
      a.href = url
      a.download = this.nameParticipant + Utils.getDatetoString() +'.webm'
      a.click()
    })
  }
  /**
   * Function setter the name of participant
   */
  setNameParticipant(name : string){
    this.nameParticipant = name;
  }
}