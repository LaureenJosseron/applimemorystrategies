import { Component, OnInit } from '@angular/core';
import * as XLSX from 'xlsx';

@Component({
  selector: 'app-excel',
  templateUrl: './excel.component.html',
  styleUrls: ['./excel.component.scss']
})
export class ExcelComponent implements OnInit {
  fileName!: string; // Name of the Excel file which will be downloaded
  sheetName!: string; // Name of the sheet within the Excel file
  wb!: XLSX.WorkBook; // Workbook object
  ws!: XLSX.WorkSheet; // Worksheet object

  constructor() { }

  ngOnInit(): void {
  }

  /**
   * Export the table data to Excel file.
   */
  exportexcel(): void {
    // Get the table element by its ID
    let element = document.getElementById('excel-table');

    // Convert the table to a worksheet object
    const ws: XLSX.WorkSheet = XLSX.utils.table_to_sheet(element);

    // Create a new workbook and add the worksheet
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    // Save the workbook to a file with the specified filename
    XLSX.writeFile(wb, this.fileName);
  }

  /**
   * Convert the table to a worksheet object.
   */
  tableToSheet() {
    let element = document.getElementById('excel-table');
    this.ws = XLSX.utils.table_to_sheet(element);
  }

  /**
   * Generate a new workbook.
   */
  generateWorkbook() {
    this.wb = XLSX.utils.book_new();
  }

  /**
   * Add the current worksheet to the workbook with the specified sheet name.
   */
  addSheetToWorkbook() {
    XLSX.utils.book_append_sheet(this.wb, this.ws, this.sheetName);
  }

  /**
   * Save the workbook to a file with the specified filename.
   */
  saveExcel() {
    XLSX.writeFile(this.wb, this.fileName);
  }
}