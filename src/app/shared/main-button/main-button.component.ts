import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-main-button',
  templateUrl: './main-button.component.html',
  styleUrls: ['./main-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MainButtonComponent {

  @Input() name!: string;
  @Input() isGreen: boolean = false;

  constructor() {}
}
