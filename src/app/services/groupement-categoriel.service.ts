import { Inject, Injectable } from '@angular/core';
import { paramGroupementCategoriel } from 'src/app/models/paramGroupementCategoriel.model';
import { configList, CONFIGLIST } from 'src/config/configList.token';

@Injectable({
  providedIn: 'root'
})
export class GroupementCategorielService {

  paramGroupementCategoriel: paramGroupementCategoriel = {
    imageNumber: this.config.GroupementCategoriel.parametres.imageNumber,
      typeList: this.config.GroupementCategoriel.parametres.typeliste,
      time: this.config.GroupementCategoriel.parametres.time,
      backGr: this.config.GroupementCategoriel.parametres.isButtonGreen
  }

  constructor( @Inject(CONFIGLIST) private config: configList) {
  }

  /**
  * Function will return the GroupementCategoriel param 
  * @param / 
  * @returns {paramGroupementCategoriel}
  */
  getParams(){
    return this.paramGroupementCategoriel;
  }

   /**
  * Function will save the param selected by the user
  * @param {paramGroupementCategoriel} param param define by user 
  * @returns {paramGroupementCategoriel}
  */
  save(param: paramGroupementCategoriel) {
    this.paramGroupementCategoriel = param;
  }

  /**
  * Function will reset by default the groupementCategoriel param from the config
  * @param / 
  * @returns {paramGroupementCategoriel}
  */
  setDefaultValues(): paramGroupementCategoriel {
    this.paramGroupementCategoriel = {
      imageNumber: this.config.GroupementCategoriel.parametres.imageNumber,
      typeList: this.config.GroupementCategoriel.parametres.typeliste,
      time: this.config.GroupementCategoriel.parametres.time,
      backGr: this.config.GroupementCategoriel.parametres.isButtonGreen
    }
    return this.paramGroupementCategoriel;
  }





}
