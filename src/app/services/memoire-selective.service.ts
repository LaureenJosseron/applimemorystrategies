import { Injectable,Inject } from '@angular/core';
import { ParamMemoireSelective } from '../models/param-memoire-selective.model';
import { configList, CONFIGLIST } from 'src/config/configList.token';

@Injectable({
  providedIn: 'root'
})
export class MemoireSelectiveService {

 ParamMemoireSelective:ParamMemoireSelective = {
      typeList: this.config.MemoireSelective.parametres.typeliste,
      type:this.config.MemoireSelective.parametres.type,
      time: this.config.MemoireSelective.parametres.time
  }

  imageNumber=6;

  constructor( @Inject(CONFIGLIST) private config: configList) {
  }

  /**
  * Function will return the MemoireSelective param 
  * @param / 
  * @returns {ParamMemoireSelective}
  */
  getParams(){
    return this.ParamMemoireSelective;
  }

   /**
  * Function will save the param selected by the user
  * @param {ParamMemoireSelective} param param define by user 
  * @returns {ParamMemoireSelective}
  */
  save(param:ParamMemoireSelective) {
    this.ParamMemoireSelective = param;
  }

  /**
  * Function will reset by default the MemoireSelective param from the config
  * @param / 
  * @returns {ParamMemoireSelective}
  */
  setDefaultValues():ParamMemoireSelective {
    this.ParamMemoireSelective = {
      typeList: this.config.MemoireSelective.parametres.typeliste,
      type:this.config.MemoireSelective.parametres.type,
      time: this.config.MemoireSelective.parametres.time
    }
    return this.ParamMemoireSelective;
  }

}
