import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { getConfigListProvider } from 'src/config/configList.token';
import { HomeComponent } from './pages/home/home.component';
import { configListData } from 'src/config/configList';
import { SharedModule } from './shared/shared.module';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    SharedModule
  ],
  providers: [
    getConfigListProvider(configListData)
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
