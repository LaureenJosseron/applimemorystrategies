import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';


const routes: Routes = [
  { path: "", component: HomeComponent },
  { path: "autorepetition", loadChildren: () => import('./pages/auto-repetition/auto-repetition.module').then((m) => m.AutoRepetitonModule) },
  { path: "groupementcategoriel", loadChildren: () => import('./pages/groupement-categoriel/groupement-categoriel.module').then((m) => m.GroupementCategorielModule) },
  { path: "memoireselective", loadChildren: () => import('./pages/memoire-selective/memoire-selective.module').then((m) => m.MemoireSelectiveModule) },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }