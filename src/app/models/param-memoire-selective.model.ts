import { configListData } from "src/config/configList";

export interface ParamMemoireSelective {
    typeList: keyof typeof configListData.MemoireSelective.images;
    type:string;
    time:number;
    
}
