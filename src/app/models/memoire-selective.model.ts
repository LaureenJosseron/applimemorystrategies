import {  configListData } from "src/config/configList"

export const TYPELIST: Record<string, { type: keyof typeof configListData.MemoireSelective.images, label: string }> = {
    liste1: {
        type: 'liste1',
        label: "Liste 1"
    },
    liste2: {
        type: 'liste2',
        label: "Liste 2"
    },
}

export const MODESMEMOIRESEL:Record<string, { type: string, label: string  }>={
    mode1: {
        type:  "CMCM",
        label: "CMCM",
    },
    // mode2: {
    //     type: "MCMC",
    //     label:"MCMC",
    // },
    mode3: {
        type: "CCMM",
        label:"CCMM",
    },
    mode4: {
        type: "MMCC",
        label:"MMCC",
    },
    mode5: {
        type: "CCCMMM",
        label:"CCCMMM",
    },
    mode6: {
        type: "MMMCCC",
        label:"MMMCCC",
    },
    mode7: {
        type: "CMCMCM",
        label:"CMCMCM",
    },
    mode8: {
        type: "MCMCMC",
        label:"MCMCMC",
    },



}