import { configListData } from "src/config/configList";

export const TYPELIST: Record<string, { type: keyof typeof configListData.AutoRepetition.images, label: string }> = {
    familiarisation1: {
        type: 'familiarisation1',
        label: "Familiarisation 1"
    },
    familiarisation2: {
        type: 'familiarisation2',
        label: "Familiarisation 2"
    },
    liste1: {
        type: 'liste1',
        label: "Liste 1"
    },
    liste2: {
        type: 'liste2',
        label: "Liste 2"
    },
    liste3: {
        type: 'liste3',
        label: "Liste 3"
    }
}

export const ORDERAUTOREP: Record<string, { type: number, label: string, order: number[] }> = {
    Ordre1: {
        type: 1,
        label: "Ordre 1",
        order: [0,1,2,3,4,5]
    },
    Ordre2: {
        type: 2,
        label: "Ordre 2",
        order: [1,5,0,4,2,3]
    },
    Ordre3: {
        type: 3,
        label: "Ordre 3",
        order: [2,0,5,1,3,4]
    },
    Ordre4: {
        type: 4,
        label: "Ordre 4",
        order: [3,4,2,5,1,0]
    },
    Ordre5: {
        type: 5,
        label: "Ordre 5",        
        order: [4,2,3,0,5,1]
    },
    Ordre6: {
        type: 6,
        label: "Ordre 6",        
        order: [5,3,1,4,0,2]
    },
}