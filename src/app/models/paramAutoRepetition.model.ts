import { configListData } from "src/config/configList";

export interface ParamAutoRepetition {
    imageNumber: number;
    timeDisplay: number;
    timeTempoBetweenCard: number;
    autoRotate: boolean;
    randomImg: boolean;
    autoRestitution: boolean;
    timeRestitution : number;
    typeSelected: keyof typeof configListData.AutoRepetition.images;
    colorchange : boolean;
    orderimage: number;
}