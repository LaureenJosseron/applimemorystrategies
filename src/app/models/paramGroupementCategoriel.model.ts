import { configListData } from "src/config/configList";

export interface paramGroupementCategoriel{
    imageNumber:number;
    typeList: keyof typeof configListData.GroupementCategoriel.images;
    time:number;
    backGr:boolean;
}