import {  configListData } from "src/config/configList"

export const TYPELIST: Record<string, { type: keyof typeof configListData.GroupementCategoriel.images, label: string }> = {
    familiarisation1: {
        type: 'familiarisation1',
        label: "Familiarisation 1"
    },
    familiarisation2: {
        type: 'familiarisation2',
        label: "Familiarisation 2"
    },
    liste1: {
        type: 'liste1',
        label: "Liste 1"
    },
    liste2: {
        type: 'liste2',
        label: "Liste 2"
    },
    liste3: {
        type: 'liste3',
        label: "Liste 3"
    }
}

export const NBIMAGELIST: Record<string, { type: number, label: string }> = {
    image8: {
        type: 8,
        label: "8 images"
    },
    image12: {
        type: 12,
        label: "12 images"
    },
    image16: {
        type: 16,
        label: "16 images"
    },
    image20: {
        type: 20,
        label: "20 images"
    },
    image24: {
        type: 24,
        label: "24 images"
    },
}