import { Component,ChangeDetectionStrategy, OnInit } from '@angular/core';
import { TYPELIST } from 'src/app/models/memoire-selective.model';

@Component({
  selector: 'app-memoire-selective',
  templateUrl: './memoire-selective.component.html',
  styleUrls: ['./memoire-selective.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MemoireSelectiveComponent implements OnInit{

  TYPELIST_VALUES = Object.values(TYPELIST);

  constructor(){
    
  }

  ngOnInit(): void {
    
  }

}
