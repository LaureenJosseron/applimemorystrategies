import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ParametreMemoireSelectiveComponent } from './parametre-memoire-selective.component';

describe('ParametreMemoireSelectiveComponent', () => {
  let component: ParametreMemoireSelectiveComponent;
  let fixture: ComponentFixture<ParametreMemoireSelectiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ParametreMemoireSelectiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ParametreMemoireSelectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
