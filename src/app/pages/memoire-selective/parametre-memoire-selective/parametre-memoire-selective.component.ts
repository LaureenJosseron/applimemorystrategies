import { Component, OnInit,ChangeDetectionStrategy } from '@angular/core';
import { TYPELIST,MODESMEMOIRESEL } from 'src/app/models/memoire-selective.model';

import { ParamMemoireSelective } from 'src/app/models/param-memoire-selective.model';
import { MemoireSelectiveService } from 'src/app/services/memoire-selective.service';

@Component({
  selector: 'app-parametre-memoire-selective',
  templateUrl: './parametre-memoire-selective.component.html',
  styleUrls: ['./parametre-memoire-selective.component.scss'],
  changeDetection:ChangeDetectionStrategy.OnPush
})

export class ParametreMemoireSelectiveComponent implements OnInit {
  TYPELIST_VALUES = Object.values(TYPELIST);
  MODES_VALUES=Object.values(MODESMEMOIRESEL);
  params!: ParamMemoireSelective;


  constructor(private service:MemoireSelectiveService) { }

  ngOnInit(): void {
    this.params = {...this.service.getParams()};
  }


  /**
  * Function will save all the MemoireSelective param by calling the service 
  * @param /
  * @returns {void}
  */
  saveParams() {
    this.service.save({...this.params});
  }


  /**
  * Function will reset all the MemoireSelective param by calling the service 
  * @param /
  * @returns {void}
  */
  resetParams() {
    this.params = {...this.service.setDefaultValues()};
  }
  
  /**
  * Function will cancel all the autoRepetition param by calling the service 
  * @param /
  * @returns {void}
  */
  cancelParams(){
    this.params = {...this.service.getParams()};
  }

}
