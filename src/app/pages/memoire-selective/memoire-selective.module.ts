import { SharedModule } from 'src/app/shared/shared.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { MemoireSelectiveComponent } from './memoire-selective.component';
import { ParametreMemoireSelectiveComponent } from './parametre-memoire-selective/parametre-memoire-selective.component';
import { ExperienceMemoireSelectiveComponent } from './experience-memoire-selective/experience-memoire-selective.component';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { FormsModule } from '@angular/forms';

export const routes = [
  { path: '', component: MemoireSelectiveComponent },
  { path: "parametre", component: ParametreMemoireSelectiveComponent },
  { path: "experience", component: ExperienceMemoireSelectiveComponent },
];

@NgModule({
  declarations: [
    MemoireSelectiveComponent,
    ParametreMemoireSelectiveComponent,
    ExperienceMemoireSelectiveComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatButtonModule,
    FormsModule,
    SharedModule,

  ]
})
export class MemoireSelectiveModule { }
