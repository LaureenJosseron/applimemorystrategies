import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExperienceMemoireSelectiveComponent } from './experience-memoire-selective.component';

describe('ExperienceMemoireSelectiveComponent', () => {
  let component: ExperienceMemoireSelectiveComponent;
  let fixture: ComponentFixture<ExperienceMemoireSelectiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExperienceMemoireSelectiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExperienceMemoireSelectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
