import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, ChangeDetectorRef, HostListener, ViewChild, Inject } from '@angular/core';
import { ModalEscapeComponent } from 'src/app/shared/modal-escape/modal-escape.component';
import Utils from 'src/app/shared/utils/utils';
import { Router } from '@angular/router';
import { configList, CONFIGLIST } from 'src/config/configList.token';
import { MemoireSelectiveService } from 'src/app/services/memoire-selective.service';
import Timer from 'src/app/shared/utils/timer';
import RecordScreen from 'src/app/shared/utils/recordScreen';
import { ExcelComponent } from 'src/app/shared/excel/excel.component';

@Component({
  selector: 'app-experience-memoire-selective',
  templateUrl: './experience-memoire-selective.component.html',
  styleUrls: ['./experience-memoire-selective.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExperienceMemoireSelectiveComponent implements OnInit, OnDestroy {

  items: Record<string, boolean | string>[] = []; // card to be displayed on the 1st line
  items2: Record<string, boolean | string>[] = [];
  style: Record<string, string> = { "width": 160 + "px", "height": 160 + "px" };
  endLifeFunction: boolean = false;
  length: number = this.service.imageNumber;
  imageDisplayed: boolean = false;
  nameParticipant: string = "";
  dataSave: Record<string, Array<Record<string, string | number>>> = {};
  displayExperience: boolean = false;
  record: any;
  indexCage: number = 0;
  indexMaison: number = 0;
  nbrClicsParImage: Record<string, number> = {};
  link: any;
  recallCage!: any;
  recallMaison!: any;
  imageCage: (string | boolean)[] = this.config.MemoireSelective.images[this.service.ParamMemoireSelective.typeList][0];
  imageMaison: (string | boolean)[] = this.config.MemoireSelective.images[this.service.ParamMemoireSelective.typeList][1];
  mode: string = this.service.ParamMemoireSelective.type; ///ex: CMCM
  modeActuel!: string; ///ex: C 
  lengthMode: number = this.service.ParamMemoireSelective.type.length;
  setOfImages!: number; ///0:cage, 1:maison
  recallState: boolean = false;
  validClics: number = 0;
  allClics: number = 0;



  timer!: any;
  timesUp: boolean = false;
  nbrTimesUp: number = 0;


  line1: string[] = ['cage', 'maison', 'cage', 'cage', 'maison', 'cage'];
  line2: string[] = ['maison', 'cage', 'maison', 'maison', 'cage', 'maison'];

  exportExcel: Array<Record<keyof typeof this.nbrClicsParImage | "temps", any>> = [];
  ExcelComponent: ExcelComponent;
  temps!: number;
  newThValue: string[] = ["totalClick", "validClick", "precision"]



  @ViewChild('modalEscape') modalEscape!: ModalEscapeComponent; /*create a view child*/


  constructor(private service: MemoireSelectiveService, private router: Router, private cd: ChangeDetectorRef, @Inject(CONFIGLIST) private config: configList) {
    this.items = Array.from({ length: this.service.imageNumber }, (v, k) => v = { "rotate": false, "src": "" as string });
    this.items2 = Array.from({ length: this.service.imageNumber }, (v, k) => v = { "rotate": false, "src": "" as string });
    this.ExcelComponent = new ExcelComponent();
  }

  ngOnDestroy(): void {
    this.endLifeFunction = true;
  }

  ngOnInit(): void {
    this.initRandomImg();
    this.record = new RecordScreen();
    this.onResize();
    this.initNbrClicsParImage();

  }


  /**
  * Function  will display the experience once the user set a particpantName    
  * @param /
  * @returns {void}
  */
  setDisplayExperience() {
    this.displayExperience = true;
    this.record.setNameParticipant(this.nameParticipant);
    this.initTimer();
    this.ExcelComponent.generateWorkbook();
  }


  /**
  * Function will init the timer at the beginning of the experience
  * @param /
  * @returns {void}
  */
  initTimer() {
    this.modeActuel = this.mode[this.nbrTimesUp];
    this.timer = new Timer(() => {

      //CLEAR LORS DU NGDESTROY

      // Turn all cards on their front
      this.items.forEach(item => (item['rotate'] = false));
      this.items2.forEach(item => (item['rotate'] = false));
      this.imageDisplayed = false;

      this.nbrTimesUp++; ///between 1 and 3
      this.timesUp = true;
      this.recall(this.nbrTimesUp);
      this.recallState = true;

      this.cd.detectChanges();


    }, this.service.ParamMemoireSelective.time * 60000);
    this.timer.start();
  }


  /**
  * Function will initiate the image list to display randomly for the experience  
  * @returns {void}
  */
  initRandomImg() {
    this.indexCage = 0;
    this.indexMaison = 0;
    Utils.shuffleArray(this.imageCage);
    Utils.shuffleArray(this.imageMaison);

    for (let i: number = 0; i < this.length; i++) {
      if (this.line1[i] === 'cage' && this.indexCage < 6) {
        let srcValue: string | boolean = this.imageCage[this.indexCage];
        this.items[i]["src"] = srcValue;
        this.indexCage++;

      }
      else if (this.line1[i] === 'maison' && this.indexMaison < 6) {
        let srcValue: string | boolean = this.imageMaison[this.indexMaison];
        this.items[i]["src"] = srcValue;
        this.indexMaison++;
      }

      if (this.line2[i] === 'cage' && this.indexCage < 6) {
        let srcValue: string | boolean = this.imageCage[this.indexCage];
        this.items2[i]["src"] = srcValue;
        this.indexCage++;
      }
      else if (this.line2[i] === 'maison' && this.indexMaison < 6) {
        let srcValue: string | boolean = this.imageMaison[this.indexMaison];
        this.items2[i]["src"] = srcValue;
        this.indexMaison++;
      }
    }
  }

  /**
  * Function to handle the click event on a card, set the rotation and alreadyDisplay states of the images on the first line
  * @param {number} index - The index of the clicked card
  * @returns {void}
  */
  async onClick(index: number) {
    if (this.recallState == true) return;
    if (this.items[index]['rotate'] === false && this.imageDisplayed === false) {
      // do roation
      this.items[index]['rotate'] = true;
      this.imageDisplayed = true;
    }
    else if (this.items[index]['rotate'] === true && this.imageDisplayed === true) {
      // do rotation 
      this.items[index]['rotate'] = false;
      this.imageDisplayed = false;

    }

  }
  /**
    * Function to handle the click event on a card in the second line
    * @param {number} index - The index of the clicked card
    * @returns {void}
    */
  async onClick2(index: number) {
    if (this.recallState == true) return;
    if (this.items2[index]['rotate'] === false && this.imageDisplayed === false) {
      this.items2[index]['rotate'] = true;
      this.imageDisplayed = true;
    }
    else if (this.items2[index]['rotate'] === true && this.imageDisplayed === true) {
      this.items2[index]['rotate'] = false;
      this.imageDisplayed = false;
    }


  }

  /**
   * Function to handle the card placed on the first line when clicking on its front
   * @param {number} index - The index of the card
   * @returns {void}
   */
  async onFront(index: number) {
    this.temps = (Date.now() - this.timer.startTime) / 1000;
    let keyName = this.extractFileName(1, this.items[index]['src'].toString(), index);
    this.nbrClicsParImage[keyName] = 1;
    this.exportExcel.push({ "temps": this.temps, ...this.nbrClicsParImage });
    this.nbrClicsParImage[keyName] = 0;

    if (this.recallState && this.indexCage > 0) {
      if (this.link === this.items[index]['src'].toString()) {
        this.validClics++;
      }
    }
    else if (!this.recallState && !this.imageDisplayed) {
      if (this.modeActuel === 'C' && this.line1[index] === 'cage') {
        this.validClics++;
      }
      else if (this.modeActuel === 'M' && this.line1[index] === 'maison') {
        this.validClics++;
      }
    }
    this.allClics++;
  }

  /**
   * Function to handle the card placed on the second line when clicking on its front
   * @param {number} index - The index of the card
   * @returns {void}
   */
  async onFront2(index: number) {
    this.temps = (Date.now() - this.timer.startTime) / 1000;
    let keyName = this.extractFileName(2, this.items2[index]['src'].toString(), index);
    this.nbrClicsParImage[keyName] = 1;
    this.exportExcel.push({ "temps": this.temps, ...this.nbrClicsParImage });
    this.nbrClicsParImage[keyName] = 0;

    if (this.recallState && this.indexCage > 0) {
      if (this.link === this.items2[index]['src'].toString()) {
        this.validClics++;
      }

    }
    else if (!this.recallState && !this.imageDisplayed) {
      if (this.modeActuel === 'C' && this.line2[index] === 'cage') {
        this.validClics++;
      }
      else if (this.modeActuel === 'M' && this.line2[index] === 'maison') {
        this.validClics++;
      }
    }
    this.allClics++;
  }


  /**
  * Function will set the width and height for the img to display by using the screen pixel   
  * @returns {void}
  */
  onResize() {
    this.style["width"] = this.style["height"] = Math.round((window.innerWidth - 25) / 10) + "px";
  }

  /**
  * Function to set an event to open a dialog on escape button     
  * @returns {void}
  */
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler() {
    if (this.modalEscape.dialog.openDialogs.length == 0) {
      this.modalEscape.openDialog();
    }
  }

  /**
  * Function to handle the recall state and update the image link
  * @param {number} index - The index of the recall
  * @returns {void}
  */
  recall(index: number) {
    this.exportExcel.push({});
    this.exportExcel.push({ "phaseMemo": "Phase mémorisation", "totalClick": "Clics total", "validClick": "Clics valides", "precision": "Précision" });
    this.exportExcel.push({ "": "", "totalClick": this.allClics, "validClick": this.validClics, "precision": (this.validClics / this.allClics) });
    this.exportExcel.push({});
    this.allClics = 0;
    this.validClics = 0;

    this.initNbrClicsParImage();
    this.indexCage = 0;
    this.indexMaison = 0;

    if (this.mode[this.nbrTimesUp - 1] === 'C') {
      this.link = 'assets/MemoireSelective/cage.jpg';
      this.setOfImages = 0;
      Utils.shuffleArray(this.imageCage);
    }
    else {
      this.link = 'assets/MemoireSelective/maison.jpg';
      this.setOfImages = 1;
      Utils.shuffleArray(this.imageMaison);
    }
  }



  /**
     * Function to change the images of the recall and set the other timers
     * @returns {void}
     */
  changeImage() {
    if (this.indexCage !== 6) {
      this.temps = (Date.now() - this.timer.startTime) / 1000;
      if (this.indexCage === 0) {
        if (this.setOfImages === 0) {
          this.link = this.imageCage[this.indexCage];
          this.indexCage++;
          let fileName = this.extractFileName(0, this.link, this.indexCage);
          for (let i = 0; i < 12; i++) {
            if (this.getObjectKeys(this.nbrClicsParImage)[i].match(fileName)) {
              fileName = this.indexCage.toString() + "_" + this.getObjectKeys(this.nbrClicsParImage)[i];
            }
          }
          
          this.exportExcel.push({ "nameImage": fileName,"time":this.temps });
        }
        else if (this.setOfImages === 1) {
          this.link = this.imageMaison[this.indexCage];
          this.indexCage++;
          let fileName = this.extractFileName(0, this.link, this.indexCage);
          for (let i = 0; i < 12; i++) {
            if (this.getObjectKeys(this.nbrClicsParImage)[i].match(fileName)) {
              fileName = this.indexCage.toString() + "_" + this.getObjectKeys(this.nbrClicsParImage)[i];
            }
          }
          this.exportExcel.push({ "nameImage": fileName,"time":this.temps });
        }
      }
      else {
        if (this.setOfImages === 0) {
          this.link = this.imageCage[this.indexCage];
          this.indexCage++;
          let fileName = this.extractFileName(0, this.link, this.indexCage);
          for (let i = 0; i < 12; i++) {
            if (this.getObjectKeys(this.nbrClicsParImage)[i].match(fileName)) {
              fileName = this.indexCage.toString() + "_" + this.getObjectKeys(this.nbrClicsParImage)[i];
            }
          }
          this.exportExcel.push({ "nameImage": fileName,"time":this.temps });
        }
        else if (this.setOfImages === 1) {
          this.link = this.imageMaison[this.indexCage];
          this.indexCage++;
          let fileName = this.extractFileName(0, this.link, this.indexCage);
          for (let i = 0; i < 12; i++) {
            if (this.getObjectKeys(this.nbrClicsParImage)[i].match(fileName)) {
              fileName = this.indexCage.toString() + "_" + this.getObjectKeys(this.nbrClicsParImage)[i];
            }
          }
          this.exportExcel.push({ "nameImage": fileName,"time":this.temps });
        }
      }
    }
    else if (this.indexCage === 6 && this.nbrTimesUp < this.lengthMode + 1) { ///this.nbrTimesUp between 1 and 3
      this.indexCage++;
      this.exportExcel.push({});
      this.exportExcel.push({ "phaseRappel": "Phase rappel", "totalClick": "Clics total", "validClick": "Clics valides", "preciison": "Précision" });
      this.exportExcel.push({ "": "", "totalClick": this.allClics, "validClick": this.validClics, "precision": (this.validClics / this.allClics) });
      this.cd.detectChanges();

      this.allClics = 0;
      this.validClics = 0;


      this.ExcelComponent.sheetName = "Essai" + this.nbrTimesUp + "_" + this.modeActuel;
      this.ExcelComponent.tableToSheet();
      this.ExcelComponent.addSheetToWorkbook();
      this.exportExcel = [];


      //Re-do the experience
      this.initRandomImg();
      this.timesUp = false;
      this.cd.detectChanges();


      this.recallState = false;
      this.timer.removeTimer();
      this.initNbrClicsParImage();
      this.modeActuel = this.mode[this.nbrTimesUp];

      if (this.nbrTimesUp === this.lengthMode) {
        //SAVEDATA

        let date = Utils.getDatetoString();
        this.ExcelComponent.fileName = this.nameParticipant + date + '.xlsx';
        this.ExcelComponent.saveExcel();
        this.timer.removeTimer();
        this.router.navigate(['/memoireselective']);
      }


      this.timer = new Timer(() => {
        ///time's up

        // Turn all cards on their front
        this.items.forEach(item => (item['rotate'] = false));
        this.items2.forEach(item => (item['rotate'] = false));
        this.imageDisplayed = false;

        this.nbrTimesUp++;

        this.recall(this.nbrTimesUp);
        this.recallState = true;
        this.timesUp = true;
        this.cd.detectChanges();

      }, this.service.ParamMemoireSelective.time * 60000);
      this.timer.start();

    }

  }

  /**
  * Function will extract File Name
  * @param {string,number} numeroLine is 1,2 or 0 if not talking about lines, i is between 0 to 5, if i=6 
  * @returns {void}
  */
  extractFileName(numeroLine: number, line: string, i: number) {
    const fileName: any = line.split('/')[3];
    const fileName2: any = line.split('/')[4];
    const image = fileName2.split('.')[0];
    let index = (i + 1).toString()
    let imagePath = '';
    if (numeroLine === 1) {
      imagePath = 'A' + index + ' ' + fileName + '/' + image;
    }
    else if (numeroLine === 2) {
      imagePath = 'B' + index + ' ' + fileName + '/' + image;
    }
    else { //name of the recall image
      imagePath = fileName + '/' + image; //i + '/' +
    }
    const keyName = `${imagePath}`;
    return keyName;

  }
  /**
    * Function to initialize the NbrClicsParImage object
    * @returns {void}
    */
  initNbrClicsParImage() {
    this.nbrClicsParImage = {};
    for (let i = 0; i < 6; i++) {
      let line: string = this.items[i]['src'].toString();
      const keyName = this.extractFileName(1, line, i);

      this.nbrClicsParImage[keyName] = 0;
    }
    for (let i = 0; i < 6; i++) {
      let line: string = this.items2[i]['src'].toString();
      const keyName = this.extractFileName(2, line, i);
      this.nbrClicsParImage[keyName] = 0;
    }


  }

  /**
     * Function to get the keys of an object
     * @param {Record<string, any>} obj - The object
     * @returns {string[]} - The keys of the object
     */
  getObjectKeys(obj: Record<string, any>): string[] {
    return Object.keys(obj);
  }

}