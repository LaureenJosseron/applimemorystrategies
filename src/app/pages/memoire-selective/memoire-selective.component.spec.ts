import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MemoireSelectiveComponent } from './memoire-selective.component';

describe('MemoireSelectiveComponent', () => {
  let component: MemoireSelectiveComponent;
  let fixture: ComponentFixture<MemoireSelectiveComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MemoireSelectiveComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MemoireSelectiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
