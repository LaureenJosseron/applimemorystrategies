import { Component, ChangeDetectionStrategy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ORDERAUTOREP, TYPELIST } from 'src/app/models/auto-repetition.model';
import { ParamAutoRepetition } from 'src/app/models/paramAutoRepetition.model';
import { AutoRepetitionService } from '../../../services/auto-repetition.service';

@Component({
  selector: 'app-parametre-auto-repetition',
  templateUrl: './parametre-auto-repetition.component.html',
  styleUrls: ['./parametre-auto-repetition.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ParametreAutoRepetitionComponent {

  ORDER_VALUES = Object.values(ORDERAUTOREP);
  params!: ParamAutoRepetition;
  titleTypeExperience : string;

  constructor(private service: AutoRepetitionService, private router: ActivatedRoute, ) {
    this.params = {...this.service.setDefaultValues(this.router.snapshot.params['id'])};
    this.titleTypeExperience = TYPELIST[this.params.typeSelected].label;
  }

  ngOnInit(): void {
    this.resetParams();
  }

  /**
   * Function will save all the autoRepetition param by calling the service 
  * @param /
  * @returns {void}
  */
  saveParams() {
    this.service.save({...this.params});
  }

  /**
  * Function will reset all the autoRepetition param by calling the service 
  * @param /
  * @returns {void}
  */
  resetParams() {
    this.params = {...this.service.setDefaultValues(this.router.snapshot.params['id'])};
  }

  /**
  * Function will cancel all the autoRepetition param by calling the service 
  * @param /
  * @returns {void}
  */
  cancelParams(){
    this.params = {...this.service.setDefaultValues(this.router.snapshot.params['id'])};
  }

  isFamiliarisation(){
    return this.params.typeSelected == 'familiarisation1' || this.params.typeSelected == 'familiarisation2' 
  }

}
