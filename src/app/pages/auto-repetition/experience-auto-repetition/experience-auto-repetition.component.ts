import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, HostListener, ViewChild, Inject, OnDestroy } from '@angular/core';
import { ModalEscapeComponent } from 'src/app/shared/modal-escape/modal-escape.component';
import Utils from 'src/app/shared/utils/utils';
import { configList, CONFIGLIST } from 'src/config/configList.token';
import { ORDERAUTOREP } from '../../../models/auto-repetition.model';
import { AutoRepetitionService } from '../../../services/auto-repetition.service';

@Component({
  selector: 'app-experience-auto-repetition',
  templateUrl: './experience-auto-repetition.component.html',
  styleUrls: ['./experience-auto-repetition.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExperienceAutoRepetitionComponent implements OnInit,OnDestroy {

  items :  Record<string, boolean| string>[] = [];
  style: Record<string,string> = { "width": 160 + "px", "height": 160 + "px" };
  endLifeFunction : boolean = false;

  @ViewChild('modalEscape') modalEscape!: ModalEscapeComponent; /*create a view child*/


  constructor(private service: AutoRepetitionService ,private cd: ChangeDetectorRef,  @Inject(CONFIGLIST) private config: configList) {
    this.items = Array.from({length: this.service.paramAutoRepetition.imageNumber}, (v, k) => v = {"rotate": false,"src":"","alreadyDisplay" : false});
  }
  ngOnDestroy(): void {
    this.endLifeFunction = true;
  }

  ngOnInit(): void {
    this.initRandomImg();
    this.onResize();
    if(this.service.paramAutoRepetition.autoRotate){
      this.autoRotate();
    }
  }

  /**
  * Function will initiate the image list to display randomly for the experience  
  * @returns {void}
  */
  initRandomImg(){
    let arrayImg: string[] = this.config.AutoRepetition.images[this.service.paramAutoRepetition.typeSelected];
    for (let i : number = 0; i < this.items.length; i++) {
      this.items[i]["src"] = arrayImg[i];
      console.log(this.items[i]["src"])
    }
    if(this.service.paramAutoRepetition.randomImg){
      Utils.shuffleArray(this.items);
    }
    else{
      let order = this.service.paramAutoRepetition.orderimage;
      let taborder;
      for (let i : number = 0; i < this.items.length; i++) {
        if(this.service.paramAutoRepetition.imageNumber == 6){
          taborder = taborder ?? ORDERAUTOREP[`Ordre${order}`].order;
          this.items[i]["src"] = arrayImg[taborder[i]];
        }
      }
    }
  }

  /**
  * Function will set the rotation and alreadyDisplay states of the image on clicking on it   
  * @param {number} index Index of the img 
  * @returns {void}
  */
  async onClick(index : number){
    if(this.items[index]['alreadyDisplay'] || this.service.paramAutoRepetition.autoRotate) return;
    this.setRotateToFalse();
    this.items[index]['rotate'] = true;
    if(this.service.paramAutoRepetition.timeDisplay != Infinity){

      await Utils.promiseTempo(this.service.paramAutoRepetition.timeDisplay);
      this.items[index]['rotate'] = false;
      this.items[index]['alreadyDisplay'] = true;

      this.cd.detectChanges();

    }
    if(this.items.filter(e => e["alreadyDisplay"] == false).length == 0){
      await this.autoReveal();
    }
  }

  /**
  * Function will set the rotation  and alreadyDisplay states of the image on clicking on it   
  * @returns {void}
  */
  setRotateToFalse(){
    let item = this.items.filter(e => e["rotate"] == true);
    if(item.length == 0) return;
    item[0]['rotate'] = false;
    item[0]['alreadyDisplay'] = true;
  }

  /**
  * Function will set the width and height for the img to display by using the screen pixel   
  * @returns {void}
  */
  onResize() {
    this.style["width"] = this.style["height"]= Math.round((window.innerWidth - 25) / 10) + "px";
  }

  /**
  * Function to set an event to open a dialog on escape button     
  * @returns {void}
  */
  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler() {
    if (this.modalEscape.dialog.openDialogs.length == 0){
      this.modalEscape.openDialog();
    }
  }

  /**
   * Function which allows automatic rotation of image
   * @returns {void}
   */
  async autoRotate(){
    await Utils.promiseTempo(this.service.paramAutoRepetition.timeDisplay);
    let cpt = 0;
    for(let item of this.items){

        item['rotate'] = true;
        this.cd.detectChanges();

        await Utils.promiseTempo(this.service.paramAutoRepetition.timeDisplay);

        item['rotate'] = false;
        item['alreadyDisplay'] = true;
        this.cd.detectChanges();

      if(cpt > 0  && this.items.filter(e => e["alreadyDisplay"] == false).length == 0){
        await this.autoReveal();
      }
      cpt++;

      await Utils.promiseTempo(this.service.paramAutoRepetition.timeTempoBetweenCard);
    }
  }

  /**
   * Function asynchrone which allows automatic reveal of image
   * @returns {void}
   */
  async autoReveal(){
    if(!this.service.paramAutoRepetition.autoRestitution) return;
    await Utils.promiseTempo(this.service.paramAutoRepetition.timeRestitution);
    for(let i = 0; i < this.items.length; i++){
      if(this.endLifeFunction) return;

      let card = document.querySelector(`.card_${i}`)?.querySelector(".flip-card-front");
      card?.classList.add('selected');
      this.cd.detectChanges();

      await Utils.promiseTempo(this.service.paramAutoRepetition.timeRestitution);

      card?.classList.remove('selected');

    }
  }

  getColorChange(){
    return this.service.paramAutoRepetition.colorchange;
  }

}
