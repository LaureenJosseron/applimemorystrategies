import { NgModule } from '@angular/core';
import { ExperienceAutoRepetitionComponent } from './experience-auto-repetition/experience-auto-repetition.component';
import { ParametreAutoRepetitionComponent } from './parametre-auto-repetition/parametre-auto-repetition.component';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { SharedModule } from 'src/app/shared/shared.module';
import { AutoRepetitionComponent } from './auto-repetition.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

export const routes = [
    { path: '', component: AutoRepetitionComponent },
    { path: "parametre/:id", component: ParametreAutoRepetitionComponent },
    { path: "experience", component: ExperienceAutoRepetitionComponent },
];

@NgModule({
    declarations: [
        AutoRepetitionComponent,
        ExperienceAutoRepetitionComponent,
        ParametreAutoRepetitionComponent,
    ],
    imports: [
        CommonModule,
        RouterModule.forChild(routes),
        MatButtonModule,
        FormsModule,
        SharedModule
    ],
})
export class AutoRepetitonModule { }
