import { Component, ChangeDetectionStrategy } from '@angular/core';
import { TYPELIST, NBIMAGELIST } from 'src/app/models/groupement-categoriel.model';
import { paramGroupementCategoriel } from 'src/app/models/paramGroupementCategoriel.model';
import { GroupementCategorielService } from '../../../services/groupement-categoriel.service';

@Component({
  selector: 'app-parametre-groupement-categoriel',
  templateUrl: './parametre-groupement-categoriel.component.html',
  styleUrls: ['./parametre-groupement-categoriel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})


export class ParametreGroupementCategorielComponent {

  TYPELIST_VALUES = Object.values(TYPELIST);
  NBIMAGELIST_VALUES = Object.values(NBIMAGELIST);
  params!: paramGroupementCategoriel;
  bool : boolean = true;
  isButtonGreen: boolean;

  constructor(private service: GroupementCategorielService) {
    this.isButtonGreen = false;
  }

  ngOnInit(): void {
    this.params = {...this.service.getParams()};
    this.params.backGr = false;
  }


  /**
  * Function will save all the GroupementCategoriel param by calling the service 
  * @param /
  * @returns {void}
  */
  saveParams() {
    this.service.save({...this.params});
  }


  /**
  * Function will reset all the GroupementCategoriel param by calling the service 
  * @param /
  * @returns {void}
  */
  resetParams() {
    this.params = {...this.service.setDefaultValues()};
  }
  
  /**
  * Function will cancel all the autoRepetition param by calling the service 
  * @param /
  * @returns {void}
  */
  cancelParams(){
    this.params = {...this.service.getParams()};
  }

  toggleButtonColor(): void {
    this.isButtonGreen = !this.isButtonGreen; // Toggle the isButtonGreen property
    this.params.backGr = !this.params.backGr;
  }

}
