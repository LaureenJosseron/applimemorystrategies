import { Component, ChangeDetectionStrategy, OnInit, Inject, HostListener, ViewChild, OnDestroy, AfterViewInit } from '@angular/core';
import { GroupementCategorielService } from '../../../services/groupement-categoriel.service';
import { CdkDragDrop } from "@angular/cdk/drag-drop";
import { Router } from '@angular/router';
import { configList, CONFIGLIST } from 'src/config/configList.token';
import { ModalEscapeComponent } from 'src/app/shared/modal-escape/modal-escape.component';
import Utils from 'src/app/shared/utils/utils';
import Timer from 'src/app/shared/utils/timer';
import RecordScreen from 'src/app/shared/utils/recordScreen';
import { ExcelComponent } from 'src/app/shared/excel/excel.component';


@Component({
  selector: 'app-experience-groupement-categoriel',
  templateUrl: './experience-groupement-categoriel.component.html',
  styleUrls: ['./experience-groupement-categoriel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ExperienceGroupementCategorielComponent implements OnInit, OnDestroy {

  @ViewChild('modalEscape') modalEscape!: ModalEscapeComponent;


  dataTable: Record<number, Array<string | number>> = {
    8: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 0, "", "", "", "", "", "", "", "", "", 0, 1, 0, "", "", "", "", "", "", "", "", 1, 0, 1, "", "", "", "", "", "", "", "", "", 1, "", "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    12: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 0, 1, 2, "", "", "", "", "", "", "", "", 2, 1, 0, "", "", "", "", "", "", "", "", 1, 0, 2, "", "", "", "", "", "", "", "", 0, 1, 2, "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    16: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", "", 5, 5, 5, "", "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", "", 5, 5, 5, "", "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    20: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
    24: ["", "", "", "", "", "", "", "", "", "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", 5, 5, 5, 5, 5, 5, 5, "", "", "", "", 5, 5, 5, 5, 5, 5, 5, "", "", "", "", "", 5, 5, 5, 5, 5, "", "", "", "", "", "", "", "", "", "", "", "", "", ""],
  };


  items!: (String | Number)[];
  style: Record<string, string> = { "width": 160 + "px", "height": 160 + "px" };
  dataSave: Record<string, Array<Record<string, string | number>>> = { "parameters": [], "mouseclick": [], "clicsParImage": [], "movements": [] };
  displayExperience: boolean = false;
  nameParticipant: string = "";
  timer: any;
  record: any;
  cardSelected: any;
  count: number = 0;
  nbrClicsParImage: Record<string, number> = {};
  durationDragDrop: number = 0;
  durationDropNewDrag: number = 0;
  dragTime!: number;
  placeTime!: number;
  newDragTime!: number;
  indexOfDragCard!: any;
  backGr!: boolean;

  exportExcel: Array<Record<any, number>> = [];//[{"temps": 0, "x": 0, "y":0 , "pression":0 }];
  ExcelComponent: ExcelComponent;

  constructor(private service: GroupementCategorielService, private router: Router, @Inject(CONFIGLIST) private config: configList) {
    this.ExcelComponent = new ExcelComponent();
  }


  ngOnInit() {
    this.onResize();
    this.initTable();
    this.initImg();
    this.record = new RecordScreen();
    this.dataSave["parameters"].push({ "nb_images": this.service.paramGroupementCategoriel.imageNumber, "list": this.service.paramGroupementCategoriel.typeList, "time": this.service.paramGroupementCategoriel.time.toString().concat(' minutes').replace('.5 minutes', ' minutes 30 secondes') });
    this.initNbrClicsParImage();
    this.backGr = this.service.paramGroupementCategoriel.backGr;
    //this.backGr = true;

  }

  ngOnDestroy(): void {
    
  }

  /**
  * Function will set the width and height for the img to display by using the screen pixel  
  * @param /
  * @returns {void}
  */
  onResize() {
    this.style["width"] = Math.round((window.innerWidth - 25) / 11) + "px";
    this.style["height"] = Math.round((window.innerHeight - 25) / 6) + "px";
  }

  /**
  * Function will set the new index of the img when dropping it on the grid
  * @param {CdkDragDrop<any>} event event on drop item
  * @returns {void}
  */
  drop(event: CdkDragDrop<any>) {
    this.updateData(event.previousContainer.data, event.container.data);
  }

  /**
    * Function will update the data after the drop event
    */
  updateData(oldCard: any, newCard: any) {
    this.saveData(oldCard, newCard);
    this.items[oldCard.index] = newCard.item;
    this.items[newCard.index] = oldCard.item;
    this.cardSelected = undefined;
  }

  /**
  * Function will init the timer at the beginning of the experience
  * @param /
  * @returns {void}
  */
  initTimer() {
    this.timer = new Timer(() => {
      //CLEAR LORS DU NGDESTROY
      this.timer.pause();
      if (this.placeTime < this.newDragTime) { /// dragged but not dropped
        this.durationDragDrop = (Date.now() - this.newDragTime) / 1000;
        let item = this.getFormatItem(this.indexOfDragCard);
        this.dataSave["movements"].push({ "oldIndex": this.indexOfDragCard, "newIndex": "", "item": item, "secondsBetweenDragDrop": this.durationDragDrop, "secondsBetweenDropNewDrag": this.durationDropNewDrag })
      }
      this.dataSave["clicsParImage"].push(this.nbrClicsParImage);
      Utils.downloadData(this.dataSave, this.nameParticipant);
      Utils.setExcelComponent(this.ExcelComponent);
      Utils.downloadExcel(this.nameParticipant);
      this.timer.removeTimer();
      this.router.navigate(['/groupementcategoriel']);


    }, this.service.paramGroupementCategoriel.time * 60000);
    this.timer.start();

  }


  /**
  * Function will initialize the img to display randomly for the experience  
  * @param /
  * @returns {void}
  */
  initImg() {
    let arrayImages = JSON.parse(JSON.stringify(this.config.GroupementCategoriel.images[this.service.paramGroupementCategoriel.typeList]));
    for (let i in arrayImages) {
      arrayImages[i] = Utils.shuffleArray(arrayImages[i])
    }
    this.items = this.items.map((item: any) => {
      if (item !== "") {
        item = arrayImages[item].shift();
      }
      return item;
    });
  }


  /**
  * Function will initialize the table
  */
  initTable() {
    let nbImages = this.service.paramGroupementCategoriel.imageNumber;
    if (nbImages < 16) {
      this.items = this.dataTable[nbImages];
    }
    else {
      this.items = Utils.createShuffleTabImages(this.dataTable[nbImages], nbImages, 11);
    }
  }


  @HostListener('document:keydown.escape', ['$event']) onKeydownHandler() {
    //CHECK SI MODAL OPEN
    if (this.modalEscape.dialog.openDialogs.length == 0) {
      this.timer.pause();
      this.modalEscape.openDialog();
    }
  }
  @HostListener('pointerdown', ['$event'])
  @HostListener('document:mousemove', ['$event']) onMouseMove(event: any) {
    if (this.displayExperience) {
      let temps = (Date.now() - this.timer.startTime) / 1000;
      this.exportExcel.push({ "temps": temps, "x": event.pageX, "y": event.pageY, "pression": (event as PointerEvent).pressure });
    }

  }


  /**
  * Function  will display the experience once the user set a particpantName    
  * @param /
  * @returns {void}
  */
  setDisplayExperience() {
    this.displayExperience = true;
    this.record.setNameParticipant(this.nameParticipant);
    this.initTimer();
    this.dragTime = Date.now();
  }


  /**
  * Function will save the old/new index of the img when dropping it on the grid
  * @param {CdkDragDrop<any>} event event on drop item
  * @returns {void}
  */
  saveData(oldCard: any, newCard: any) {
    this.placeTime = Date.now();
    this.durationDragDrop = (this.placeTime - this.newDragTime) / 1000;
    this.dragTime = this.newDragTime;

    let newCaseIndex = this.getCase(newCard.index);
    let oldCaseIndex = this.getCase(oldCard.index);

    let item = this.getFormatItem(this.items[oldCard.index]);

    this.dataSave["movements"].push({ "oldIndex": oldCaseIndex, "newIndex": newCaseIndex, "item": item, "secondsBetweenDragDrop": this.durationDragDrop, "secondsBetweenDropNewDrag": this.durationDropNewDrag })
    if (this.items[newCard.index] != "" && newCaseIndex != oldCaseIndex) {
      let itemSwap = this.getFormatItem(this.items[newCard.index]);
      this.dataSave["movements"].push({ "oldIndex": newCaseIndex, "newIndex": oldCaseIndex, "item": itemSwap, "secondsBetweenDragDrop": this.durationDragDrop, "secondsBetweenDropNewDrag": this.durationDropNewDrag })
    }


  }


  /**
  * Function will return the letter and index of new/previous image position on the grid
  * @param {number} val image index
  * @returns {string}
  */
  getCase(val: number) {
    let alphabet: string = 'abcdefghijklmnopqrstuvwxyz';
    let letter: string = alphabet[Math.trunc(val / 11)].toUpperCase();
    let index: number = val % 11 + 1;
    return letter + index;
  }


  /**
  * Function will return path of the current image
  * @param {number} val image index
  * @returns {string}
  */
  getFormatItem(val: any) {
    let liste = this.service.paramGroupementCategoriel.typeList;
    let path = `assets/GroupementCategoriel/${liste}/`;
    return val.replace(path, "").replace(".jpg", "");
  }

  /**
   * Function will handle the click event on a card
   */
  onClick(card: any) {
    if (!this.cardSelected) {
      this.cardSelected = card;
    }
    else {
      this.updateData(this.cardSelected, card);
    }
  }

  /**
    * Function will handle the drag event
    */
  onDrag(event: PointerEvent, card: { item: any, index: number }) {
    this.indexOfDragCard = this.getCase(card['index']);


    this.newDragTime = Date.now();
    let firstDrag: Number = 0;
    if (this.dragTime !== this.newDragTime && firstDrag === 0) {
      this.durationDropNewDrag = (this.newDragTime - this.dragTime) / 1000;
      firstDrag = 1;
    }
    else {
      this.durationDropNewDrag = (this.placeTime - this.newDragTime) / 1000;
    }

    this.count++;
    for (let key in this.nbrClicsParImage) {
      if (key === this.getFormatItem(this.items[card.index])) {
        this.nbrClicsParImage[key] += 1;
      }
    }
  }
  /**
    * Function will initialize the number of clicks per image
    */
  initNbrClicsParImage() {
    let copyList = JSON.parse(JSON.stringify(this.config.GroupementCategoriel.images[this.service.paramGroupementCategoriel.typeList]));
    for (let key in copyList) {
      const lines = copyList[key];
      for (const line of lines) {
        const fileName: any = line.split('/')[3];
        const fileName2: any = line.split('/')[4];
        const image = fileName2.split('.')[0];
        const imagePath = fileName + '/' + image;
        const keyName = `${imagePath}`
        this.nbrClicsParImage[keyName] = 0;
      }
    }
  }
}