import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'app-groupement-categoriel',
  templateUrl: './groupement-categoriel.component.html',
  styleUrls: ['./groupement-categoriel.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GroupementCategorielComponent implements OnInit {

  constructor() { }

  ngOnInit(): void { }
  
}
