# applimemorystrategies

You can try by yourself the application on : https://laureenjosseron.gitlab.io/applimemorystrategies/

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 13.3.3.

## Install & launch


```bash
  #Clone the project
  git clone https://gitlab.com/LaureenJosseron/applimemorystrategies.git
```

### Script 

```bash
  #Go to the repository
  cd applimemorystrategies/scripts
```

```bash
  #Run the script
  ./installAndLaunch.sh
```
Navigate to `http://localhost:4200/`.

### Commands lines


```bash
  #Go to the repository
  cd applimemorystrategies
```

```bash
  #Install dependancies
  npm ci
```

```bash
  #Run the local project
  npm run start
```

Navigate to `http://localhost:4200/`.
